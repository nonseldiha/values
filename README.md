# Values

Return multiple [values](http://www.lispworks.com/documentation/HyperSpec/Body/f_values.htm) from a single function call.

Include in your `deps.edn` with coordinates

``` clojure
nonseldiha/values {:git/url "https://gitlab.com/nonseldiha/values.git" :sha "..."}
```

## Why would I want this?

Consider getting a value from a map: 

``` clojure
(:x my-map)
```

This will return `nil` when `:x` is not in the map but also when the map
contains the key with a `nil` value. For 95% of usages we don't care, but where
we do we have to be explicit:

``` clojure
(if (contains? my-map :x)
  (if-let [x (:x my-map)] 
    (println "found with" (pr-str x))
    (println "found but nil"))
  (println "not found"))
```

Common Lisp's `values` allows to fix this with a simpler API. Imagine we have
`get+` that behaves like `clojure.core/get` but also returns a secondary value
signifying whether the key is in `my-map`. Then we can `mvb`
(multiple-value-bind) the values directly:

``` clojure
(mvb [x found?] (:x my-map)
  (cond (and found? x) (println "found with" (pr-str x))
        found?         (println "found but nil")
        :else          (println "not found")))
```

Defining such a function is trivial with this library:

``` clojure
(require '[nonseldiha.values :as nv])
(nv/defn get+ [m k] (nv/values (get m k) (contains? m k)))
```

Basically any function that

- has 2 or more return values
- but typically we only care about the first one

becomes simpler with this approach.


## So now I always have to wrap `get+` in an `mvb` call, not cool..

That would indeed be horrible. Luckily that isn't the case:

``` clojure
(get+ {:a 1} :a) ; => 1
```

## There must be some performance penalty for this..

When calling the function as usual there's no overhead. The additional values
aren't even calculated! When using `mvb` we wrap the result in a vector.

## Sounds great! Any gotchas I should know about?

Sure. What `nv/defn` does internally is define `get+` **and** `get+__values__`.
So there is some compilation and code size overhead. `mvb` then rewrites your
`get+` call to be `get+__values__`. The rest is easy to imagine. You can look
into the source code, the implementation is really short.

## Are you planning to add some features later?

Yes. Some things missing, in random order:

- clojurescript support
- returning an array instead of a vector, for better performance
- maybe add [multiple-value-list](http://www.lispworks.com/documentation/HyperSpec/Body/m_mult_1.htm) and friends.
- clj-kondo rule for `mvb`

## Is this implementation equivalent to what Common Lisp provides?

No. It's easy for a CL programmer to come up with examples that won't work with
this simple implementaiton. But it's still useful.
