(ns nonseldiha.values-test
  (:require [clojure.test :as t]
            [nonseldiha.values :as nv]))
(nv/defn --test-fn [x] (nv/values x x))
(t/deftest it-works
  (t/is (= 1 (--test-fn 1)))
  (t/is (= [1 1] (nv/mvb [x y] (--test-fn 1) [x y]))))
(nv/defn --thrower [] (nv/values 1 (throw (ex-info "this is fine" {}))))
(t/deftest additional-values-arent-calculated-when-function-is-called-normally
  (t/is (= 1 (--thrower))))
