(ns nonseldiha.values
  (:refer-clojure :exclude [defn])
  (:require [clojure [core :as cc] [walk :as walk]]))
(cc/defn- valuify [sym] (symbol (namespace sym) (str (name sym) "__values__")))
(cc/defn- values-form? [form] (and (list? form) (symbol? (first form)) (= (resolve `values) (resolve (first form)))))
(cc/defn defn% [args]
  `(do (cc/defn ~(valuify (first args)) ~@(walk/postwalk #(if (values-form? %) (vec (rest %)) %) (rest args)))
       (cc/defn ~@(walk/postwalk #(if (values-form? %) (second %) %) args))))
(defmacro defn [& args] (defn% args))
(cc/defn- mvb% [binds call body] `(let [~binds (~(valuify (first call)) ~@(rest call))] ~@body))
(defmacro mvb {:style/indent 2} [binds call & body] (mvb% binds call body))
(declare values)
